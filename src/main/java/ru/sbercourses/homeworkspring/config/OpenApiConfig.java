package ru.sbercourses.homeworkspring.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {

    @Bean
    public OpenAPI onlineCinemaProject() {
        return new OpenAPI()
                .info(new Info()
                        .description("Сайт онлайн-кинотеатра")
                        .title("Онлайн-кинотеатр")
                        .version("v0.1")
                        .contact(new Contact().name("Eugene Z."))
                );
    }

}

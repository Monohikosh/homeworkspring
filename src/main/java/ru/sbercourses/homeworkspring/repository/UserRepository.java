package ru.sbercourses.homeworkspring.repository;

import org.springframework.stereotype.Repository;
import ru.sbercourses.homeworkspring.model.User;

@Repository
public interface UserRepository extends GenericRepository<User> {


    User findUserByLogin(String username);

}


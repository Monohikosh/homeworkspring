package ru.sbercourses.homeworkspring.repository;

import org.springframework.stereotype.Repository;
import ru.sbercourses.homeworkspring.model.Director;
import ru.sbercourses.homeworkspring.model.Film;
import ru.sbercourses.homeworkspring.model.Genre;

import java.util.List;
import java.util.Set;

@Repository
public interface FilmRepository extends GenericRepository<Film> {

    List<Film> findAllByTitleOrGenreOrCountry(String title, Genre genre, String country);
    Set<Film> findAllByIdIn(Set<Long> ids);

    List<Film> findAllByTitle(String title);

}


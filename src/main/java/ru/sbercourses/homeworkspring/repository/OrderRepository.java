package ru.sbercourses.homeworkspring.repository;

import org.springframework.stereotype.Repository;
import ru.sbercourses.homeworkspring.model.Film;
import ru.sbercourses.homeworkspring.model.Order;

import java.util.List;

@Repository
public interface OrderRepository extends GenericRepository<Order> {


}


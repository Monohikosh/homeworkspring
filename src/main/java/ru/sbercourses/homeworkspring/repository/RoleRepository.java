package ru.sbercourses.homeworkspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sbercourses.homeworkspring.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {



}

package ru.sbercourses.homeworkspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sbercourses.homeworkspring.model.Director;

import java.util.List;
import java.util.Set;

@Repository
public interface DirectorRepository extends GenericRepository<Director> {

    Set<Director> findAllByIdIn(Set<Long> ids);

    List<Director> findAllByDirectorsFIO(String fio);

}

package ru.sbercourses.homeworkspring.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.sbercourses.homeworkspring.model.Director;
import ru.sbercourses.homeworkspring.repository.DirectorRepository;
import ru.sbercourses.homeworkspring.repository.GenericRepository;

import java.util.List;

@Service
public class DirectorService extends GenericService<Director> {

    private final DirectorRepository directorRepository;

    protected DirectorService(DirectorRepository repository) {
        super(repository);
        this.directorRepository = repository;
    }

    public List<Director> searchByDirectorFIO(String fio) {
        return directorRepository.findAllByDirectorsFIO(fio);
    }

    public Page<Director> listAllPaginated(Pageable pageable) {
        return directorRepository.findAll(pageable);
    }
}

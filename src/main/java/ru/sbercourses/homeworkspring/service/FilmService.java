package ru.sbercourses.homeworkspring.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.sbercourses.homeworkspring.model.Director;
import ru.sbercourses.homeworkspring.model.Film;
import ru.sbercourses.homeworkspring.model.Genre;
import ru.sbercourses.homeworkspring.model.Order;
import ru.sbercourses.homeworkspring.repository.DirectorRepository;
import ru.sbercourses.homeworkspring.repository.FilmRepository;
import ru.sbercourses.homeworkspring.repository.OrderRepository;

import java.util.List;

@Service
public class FilmService extends GenericService<Film> {

    private final FilmRepository repository;
    private final DirectorRepository directorRepository;

    protected FilmService(FilmRepository repository, DirectorRepository directorRepository) {
        super(repository);
        this.repository = repository;
        this.directorRepository = directorRepository;
    }

    public Film addDirectors(Long filmID, Long directorID) {
        Film film = repository.findById(filmID).get();
        Director director = directorRepository.findById(directorID).get();

        film.getDirectors().add(director);

        return repository.save(film);
    }

    public List<Film> search(String title, Genre genre, String country) {
        return repository.findAllByTitleOrGenreOrCountry(
                title,
                genre,
                country
        );
    }

    public List<Film> searchByTitle(String title) {
        return repository.findAllByTitle(title);
    }

    public Page<Film> listAllPaginated(Pageable pageable) {
        return repository.findAll(pageable);
    }


}

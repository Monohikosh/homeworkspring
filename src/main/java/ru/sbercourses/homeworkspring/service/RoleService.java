package ru.sbercourses.homeworkspring.service;

import org.springframework.stereotype.Service;
import ru.sbercourses.homeworkspring.model.Role;
import ru.sbercourses.homeworkspring.repository.RoleRepository;

import java.util.List;

@Service
public class RoleService {

    private final RoleRepository repository;

    public RoleService(RoleRepository repository) {
        this.repository = repository;
    }

    public List<Role> listAll() {
        return repository.findAll();
    }

    public Role getOne(Long id) {
        return repository.findById(id).orElseThrow();
    }

    public Role create(Role role) {
        return repository.save(role);
    }

    public Role update(Role role) {
        return repository.save(role);
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }

}

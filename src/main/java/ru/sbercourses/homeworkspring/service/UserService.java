package ru.sbercourses.homeworkspring.service;

import org.springframework.stereotype.Service;
import ru.sbercourses.homeworkspring.model.User;
import ru.sbercourses.homeworkspring.repository.UserRepository;


@Service
public class UserService extends GenericService<User> {

    private final UserRepository repository;
    private final RoleService roleService;

    protected UserService(UserRepository repository, RoleService roleService) {
        super(repository);
        this.repository = repository;
        this.roleService = roleService;
    }

    @Override
    public User create(User user) {
        user.setRole(roleService.getOne(1L));
        return repository.save(user);
    }
}

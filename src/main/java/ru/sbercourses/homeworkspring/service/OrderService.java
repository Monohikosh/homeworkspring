package ru.sbercourses.homeworkspring.service;

import org.springframework.stereotype.Service;
import ru.sbercourses.homeworkspring.model.Film;
import ru.sbercourses.homeworkspring.model.Order;
import ru.sbercourses.homeworkspring.model.User;
import ru.sbercourses.homeworkspring.repository.FilmRepository;
import ru.sbercourses.homeworkspring.repository.OrderRepository;
import ru.sbercourses.homeworkspring.repository.UserRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService extends GenericService<Order> {

    private final OrderRepository repository;
    private final FilmRepository filmRepository;
    private final UserRepository userRepository;

    protected OrderService(OrderRepository repository, FilmRepository filmRepository, UserRepository userRepository) {
        super(repository);
        this.repository = repository;
        this.filmRepository = filmRepository;
        this.userRepository = userRepository;
    }

    public Order buyFilm(Long filmID, Order order, Long userID) {
        order.setFilm(filmRepository.findById(filmID).get());
        order.setPurchase(true);
        order.setUser(userRepository.findById(userID).get());
        return repository.save(order);
    }

    public Order toRent(Long filmID, Order order, Long userID) {
        order.setFilm(filmRepository.findById(filmID).get());
        order.setRentPeriod(LocalDateTime.now().plusDays(30));
        order.setRentDate(LocalDateTime.now());
        order.setUser(userRepository.findById(userID).get());
        return repository.save(order);
    }

}

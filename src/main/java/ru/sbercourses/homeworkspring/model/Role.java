package ru.sbercourses.homeworkspring.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "roles")
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "roles_seq", allocationSize = 1)
public class Role extends GenericModel {

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

}

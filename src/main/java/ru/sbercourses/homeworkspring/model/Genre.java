package ru.sbercourses.homeworkspring.model;

public enum Genre {

    FANTASY("Фантастика"),
    COMEDY("Комедия"),
    DRAMA("Драма"),
    SCIENCE_FICTION("Научная фантастика");

    private final String genreText;

    Genre(String genreText) {
        this.genreText = genreText;
    }

    public String getGenreText() {
        return this.genreText;
    }
}

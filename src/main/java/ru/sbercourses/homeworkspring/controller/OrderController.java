package ru.sbercourses.homeworkspring.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sbercourses.homeworkspring.dto.OrderDto;
import ru.sbercourses.homeworkspring.mapper.OrderMapper;
import ru.sbercourses.homeworkspring.model.Order;
import ru.sbercourses.homeworkspring.service.OrderService;

@RestController
@RequestMapping("/order")
public class OrderController extends GenericController<Order, OrderDto> {

    private final OrderService service;

    protected OrderController(OrderService service, OrderMapper mapper) {
        super(service, mapper);
        this.service = service;
    }

    @PostMapping("/buy-film/{filmID}/{userID}")
    public ResponseEntity<Order> buyFilm(@PathVariable Long filmID,
                                         @RequestBody Order order,
                                         @PathVariable Long userID) {
        return ResponseEntity.status(HttpStatus.OK).body(service.buyFilm(filmID, order, userID));
    }

    @PostMapping("/to-rent/{filmID}/{userID}")
    public ResponseEntity<Order> toRentFilm(@PathVariable Long filmID,
                                            @RequestBody Order order,
                                            @PathVariable Long userID) {
        return ResponseEntity.status(HttpStatus.OK).body(service.toRent(filmID, order, userID));
    }


}

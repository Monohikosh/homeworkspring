package ru.sbercourses.homeworkspring.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sbercourses.homeworkspring.model.Role;
import ru.sbercourses.homeworkspring.service.RoleService;

@RestController
@RequestMapping("/role")
public class RoleController {

    private final RoleService service;

    public RoleController(RoleService service) {
        this.service = service;
    }

    @Operation(description = "Получить список всех записей", method = "GetAll")
    @GetMapping("/list")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.status(HttpStatus.OK).body(service.listAll());
    }

    @Operation(description = "Получить запись по ID", method = "GetOne")
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(service.getOne(id));
    }

    @Operation(description = "Создать запись", method = "Create")
    @PostMapping("")
    public ResponseEntity<?> create(@RequestBody Role role) {
        return ResponseEntity.status(HttpStatus.OK).body(service.create(role));
    }

    @Operation(description = "Обновить запись по ID", method = "Update")
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Role role, @PathVariable Long id) {
        role.setId(id);
        return ResponseEntity.status(HttpStatus.OK).body(service.update(role));
    }

    @Operation(description = "Удалить запись по ID", method = "Delete")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }
}

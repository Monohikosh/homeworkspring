package ru.sbercourses.homeworkspring.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sbercourses.homeworkspring.dto.FilmDto;
import ru.sbercourses.homeworkspring.mapper.FilmMapper;
import ru.sbercourses.homeworkspring.model.Film;
import ru.sbercourses.homeworkspring.model.Genre;
import ru.sbercourses.homeworkspring.service.FilmService;

import java.util.List;

@RestController
@RequestMapping("/film")
public class FilmController extends GenericController<Film, FilmDto> {

    private final FilmService service;

    protected FilmController(FilmService service, FilmMapper mapper) {
        super(service, mapper);
        this.service = service;
    }

    @RequestMapping(value = "/add-directors", method = RequestMethod.PUT)
    public ResponseEntity<Film> addDirector(@RequestParam Long filmID, @RequestParam Long directorID) {
        return ResponseEntity.status(HttpStatus.OK).body(service.addDirectors(filmID, directorID));
    }

    @GetMapping("/search")
    public List<Film> seacrh(
            @RequestParam(value = "title", required = false) String title,
            @RequestParam(value = "genre", required = false) Genre genre,
            @RequestParam(value = "country", required = false) String country) {
        return service.search(title, genre, country);
    }

}

package ru.sbercourses.homeworkspring.controller;

import org.springframework.web.bind.annotation.*;
import ru.sbercourses.homeworkspring.dto.UserDto;
import ru.sbercourses.homeworkspring.mapper.UserMapper;
import ru.sbercourses.homeworkspring.model.User;
import ru.sbercourses.homeworkspring.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController extends GenericController<User, UserDto> {

    protected UserController(UserService service, UserMapper mapper) {
        super(service, mapper);
    }

}

package ru.sbercourses.homeworkspring.controller;

import org.springframework.web.bind.annotation.*;
import ru.sbercourses.homeworkspring.dto.DirectorDto;
import ru.sbercourses.homeworkspring.dto.DirectorWithFilmsDto;
import ru.sbercourses.homeworkspring.mapper.DirectorMapper;
import ru.sbercourses.homeworkspring.mapper.DirectorWithFilmsMapper;
import ru.sbercourses.homeworkspring.model.Director;
import ru.sbercourses.homeworkspring.service.DirectorService;

import java.util.List;

@RestController
@RequestMapping("/director")
public class DirectorController extends GenericController<Director, DirectorDto> {

    private final DirectorService service;
    private final DirectorWithFilmsMapper directorWithFilmsMapper;

    protected DirectorController(DirectorMapper mapper, DirectorService service, DirectorWithFilmsMapper directorWithFilmsMapper) {
        super(service, mapper);
        this.service = service;
        this.directorWithFilmsMapper = directorWithFilmsMapper;
    }

    @GetMapping("director-films")
    public List<DirectorWithFilmsDto> getAuthorsWithBooks() {
        return service.listAll().stream().map(directorWithFilmsMapper::toDto).toList();
    }
}
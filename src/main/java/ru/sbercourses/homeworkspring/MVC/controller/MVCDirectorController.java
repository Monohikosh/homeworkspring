package ru.sbercourses.homeworkspring.MVC.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.sbercourses.homeworkspring.dto.DirectorDto;
import ru.sbercourses.homeworkspring.mapper.DirectorMapper;
import ru.sbercourses.homeworkspring.model.Director;
import ru.sbercourses.homeworkspring.service.DirectorService;

import java.util.List;

@Slf4j
@Controller
@RequestMapping("/directors")
public class MVCDirectorController {

    private final DirectorService service;
    private final DirectorMapper mapper;

    public MVCDirectorController(DirectorService service, DirectorMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping("")
    public String getAll(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "size", defaultValue = "5") int pageSize,
            Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "directorsFIO"));
        Page<Director> directorPage = service.listAllPaginated(pageRequest);
        List<DirectorDto> directorDtos = directorPage
                .stream()
                .map(mapper::toDto)
                .toList();
        model.addAttribute("directors",
                new PageImpl<>(directorDtos, pageRequest, directorPage.getTotalElements()));
        return "directors/viewAllDirectors";
    }

    @GetMapping("/add")
    public String create() {
        return "directors/addDirector";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("directorForm") DirectorDto directorDto) {
        service.create(mapper.toEntity(directorDto));
        return "redirect:/directors";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        service.delete(id);
        return "redirect:/directors";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id, Model model) {
        model.addAttribute("director", mapper.toDto(service.getOne(id)));
        return "directors/updateDirector"; // path to html file
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("directorForm") DirectorDto directorDto) {
        service.update(mapper.toEntity(directorDto));
        return "redirect:/directors";
    }

    @PostMapping("/search")
    public String searchByDirectorFIO(@ModelAttribute("searchDirectors") DirectorDto directorDto, Model model) {
        if (directorDto.getDirectorsFIO().trim().equals("")) {
            model.addAttribute("director", mapper.toDtos(service.listAll()));
        } else {
            model.addAttribute("director", service.searchByDirectorFIO(directorDto.getDirectorsFIO()));
        }
        return "directors/viewAllDirectors";
    }
}
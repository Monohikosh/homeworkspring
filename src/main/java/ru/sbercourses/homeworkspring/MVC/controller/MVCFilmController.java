package ru.sbercourses.homeworkspring.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.sbercourses.homeworkspring.dto.FilmDto;
import ru.sbercourses.homeworkspring.mapper.FilmMapper;
import ru.sbercourses.homeworkspring.model.Film;
import ru.sbercourses.homeworkspring.service.FilmService;

import java.util.List;

@Controller
@RequestMapping("/films")
public class MVCFilmController {

    private final FilmService service;
    private final FilmMapper mapper;

    public MVCFilmController(FilmService service, FilmMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping("")
    public String getAll(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "size", defaultValue = "5") int pageSize,
            Model model
    ) {
        PageRequest pageRequest = PageRequest.of(page-1, pageSize, Sort.by(Sort.Direction.ASC, "title"));
        Page<Film> filmPage = service.listAllPaginated(pageRequest);
        List<FilmDto> filmDtos = filmPage
                .stream()
                .map(mapper::toDto)
                .toList();
        model.addAttribute("films", new PageImpl<>(filmDtos, pageRequest, filmPage.getTotalElements()));
        return "films/viewAllFilms";
    }

    @GetMapping("/add")
    public String create() {
        return "films/addFilm"; //путь до html файла
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("filmForm") FilmDto filmDto) {
        service.create(mapper.toEntity(filmDto));
        return "redirect:/films";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        service.delete(id);
        return "redirect:/films";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id, Model model) {
        model.addAttribute("film", mapper.toDto(service.getOne(id)));
        return "films/updateFilm"; //путь до html файла
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("filmForm") FilmDto filmDto) {
        service.update(mapper.toEntity(filmDto));
        return "redirect:/films";
    }

    @PostMapping("/search")
    public String searchByTitle(Model model, @ModelAttribute("searchFilms") FilmDto filmDto) {
        if (filmDto.getTitle().trim().equals("")) {
            model.addAttribute("films", mapper.toDtos(service.listAll()));
        } else {
            model.addAttribute("films", service.searchByTitle(filmDto.getTitle()));
        }
        return "films/viewAllFilms";
    }
}

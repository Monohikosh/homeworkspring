package ru.sbercourses.homeworkspring;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class HomeworkSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeworkSpringApplication.class, args);
		log.info("Swagger-ui run on: http://localhost:9090/swagger-ui/index.html");
	}

}
package ru.sbercourses.homeworkspring.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.sbercourses.homeworkspring.dto.OrderDto;
import ru.sbercourses.homeworkspring.model.Order;
import ru.sbercourses.homeworkspring.repository.FilmRepository;
import ru.sbercourses.homeworkspring.repository.UserRepository;

import javax.annotation.PostConstruct;

@Component
public class OrderMapper extends GenericMapper<Order, OrderDto> {

    private final FilmRepository filmRepository;
    private final UserRepository userRepository;

    protected OrderMapper(ModelMapper mapper, FilmRepository filmRepository, UserRepository userRepository) {
        super(mapper, Order.class, OrderDto.class);
        this.filmRepository = filmRepository;
        this.userRepository = userRepository;
    }

    @PostConstruct
    public void setupMapper() {
        super.mapper.createTypeMap(Order.class, OrderDto.class)
                .addMappings(m -> m.skip(OrderDto::setUserID)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(OrderDto::setFilmID)).setPostConverter(toDtoConverter());
        super.mapper.createTypeMap(OrderDto.class, Order.class)
                .addMappings(m -> m.skip(Order::setUser)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Order::setFilm)).setPostConverter(toEntityConverter());
    }

    @Override
    void mapSpecificFields(Order source, OrderDto destination) {
        destination.setUserID(source.getUser().getId());
        destination.setFilmID(source.getFilm().getId());
    }

    @Override
    void mapSpecificFields(OrderDto source, Order destination) {
        destination.setFilm(filmRepository.findById(source.getFilmID()).orElseThrow());
        destination.setUser(userRepository.findById(source.getUserID()).orElseThrow());
    }
}
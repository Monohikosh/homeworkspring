package ru.sbercourses.homeworkspring.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.sbercourses.homeworkspring.dto.UserDto;
import ru.sbercourses.homeworkspring.model.User;

@Component
public class UserMapper extends GenericMapper<User, UserDto> {

    protected UserMapper(ModelMapper mapper) {
        super(mapper, User.class, UserDto.class);
    }
}

package ru.sbercourses.homeworkspring.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.sbercourses.homeworkspring.dto.FilmWithDirectorsDto;
import ru.sbercourses.homeworkspring.model.Film;
import ru.sbercourses.homeworkspring.model.GenericModel;
import ru.sbercourses.homeworkspring.repository.DirectorRepository;

import javax.annotation.PostConstruct;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class FilmWithDirectorsMapper extends GenericMapper<Film, FilmWithDirectorsDto> {

    private final ModelMapper mapper;
    private final DirectorRepository directorRepository;

    protected FilmWithDirectorsMapper(ModelMapper mapper, DirectorRepository directorRepository) {
        super(mapper, Film.class, FilmWithDirectorsDto.class);
        this.mapper = mapper;
        this.directorRepository = directorRepository;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(Film.class, FilmWithDirectorsDto.class)
                .addMappings(m -> m.skip(FilmWithDirectorsDto::setDirectorIds)).setPostConverter(toDtoConverter());
        mapper.createTypeMap(FilmWithDirectorsDto.class, Film.class)
                .addMappings(m -> m.skip(Film::setDirectors)).setPostConverter(toEntityConverter());
    }

    @Override
    void mapSpecificFields(Film source, FilmWithDirectorsDto destination) {
        destination.setDirectorIds(getIds(source));
    }

    private Set<Long> getIds(Film film) {
        return Objects.isNull(film) || Objects.isNull(film.getId())
                ? null
                : film.getDirectors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }

    @Override
    void mapSpecificFields(FilmWithDirectorsDto source, Film destination) {
        destination.setDirectors(directorRepository.findAllByIdIn(source.getDirectorIds()));
    }

}
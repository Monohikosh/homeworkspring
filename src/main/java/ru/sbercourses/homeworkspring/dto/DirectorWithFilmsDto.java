package ru.sbercourses.homeworkspring.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DirectorWithFilmsDto extends DirectorDto {

    private Set<FilmDto> films;

}
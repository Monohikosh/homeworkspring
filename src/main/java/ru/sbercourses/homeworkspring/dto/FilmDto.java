package ru.sbercourses.homeworkspring.dto;

import lombok.*;
import ru.sbercourses.homeworkspring.model.Genre;

import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FilmDto extends GenericDto {

    private String title;
    private Integer premierYear;
    private String country;
    private Genre genre;
    private Set<Long> directorIds;

}

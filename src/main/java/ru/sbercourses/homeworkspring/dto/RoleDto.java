package ru.sbercourses.homeworkspring.dto;

import lombok.*;
import ru.sbercourses.homeworkspring.model.Role;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class RoleDto extends GenericDto {

    private Long id;
    private String title;
    private String description;

    public RoleDto(Role role) {
        this.id = role.getId();
        this.title = role.getTitle();
        this.description = role.getDescription();
    }

}

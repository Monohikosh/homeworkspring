package ru.sbercourses.homeworkspring.dto;

import lombok.*;
import ru.sbercourses.homeworkspring.model.Film;
import ru.sbercourses.homeworkspring.model.User;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto extends GenericDto {

    private Long userID;
    private Long filmID;
    private LocalDateTime rentDate;
    private LocalDateTime rentPeriod;
    private Boolean purchase;

}

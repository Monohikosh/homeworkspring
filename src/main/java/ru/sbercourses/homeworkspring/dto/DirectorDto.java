package ru.sbercourses.homeworkspring.dto;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DirectorDto extends GenericDto {

    private String directorsFIO;
    private String position;
    private Set<Long> filmIds;

}
